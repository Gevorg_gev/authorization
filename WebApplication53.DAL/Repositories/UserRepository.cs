﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication53.Core.Entities;
using WebApplication53.Core.Repositories;

namespace WebApplication53.DAL.Repositories
{
    public class UserRepository : SqlRepositoryBase<User>, IUserRepository
    {
        public UserRepository(FreightDbContext freightDbContext) : base(freightDbContext) 
        {

        }
    }
}
