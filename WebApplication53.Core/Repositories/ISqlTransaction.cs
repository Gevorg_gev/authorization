﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication53.Core.Repositories
{
    public interface ISqlTransaction
    {
        void Commit();
        void Rollback(); 
    }
}
