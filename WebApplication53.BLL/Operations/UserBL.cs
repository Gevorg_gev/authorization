﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http; 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication53.Core.Exceptions;
using WebApplication53.Core.Models;
using WebApplication53.Core.Operations;
using WebApplication53.Core.Repositories;
using WebApplication53.DAL.Repositories;
using WebApplication53.Core.Entities;
using System.Security.Claims;
using System.Linq;

namespace WebApplication53.BLL.Operations
{
    public class UserBL : IUserBL
    {
        private readonly IRepositoryManager _repositories;

        public UserBL(IRepositoryManager repositories)
        {
            _repositories = repositories;
        } 

        public Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext)
        {
            return httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
         
        public Task LogOutAsync(HttpContext httpContext)
        {
            return httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public async Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext)
        {
            var users = _repositories.Users.GetWhere(x => x.UserName == registerModel.UserName);

            if (users.Any()) 
            {
                throw new LogicException("Username is already taken");
            }

            var user = new User
            {
                UserName = registerModel.UserName,
                Password = registerModel.Password
            };

            _repositories.Users.Add(user);  

            await _repositories.SaveChangesAsync(); 

            await Authenticate(user, httpContext);  

            return new UserViewModel
            {
                Id = user.Id,
                Username = user.UserName 
            };
        }

        private async Task Authenticate(User user, HttpContext httpContext)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim("Id",user.Id.ToString())
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}
