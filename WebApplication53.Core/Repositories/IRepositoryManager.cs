﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication53.Core.Repositories
{
    public interface IRepositoryManager
    {
        IFreightRepository Freights { get; }

        IUserRepository Users { get; }

        int SaveChanges();
        Task<int> SaveChangesAsync();
        ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted); 
    }
}