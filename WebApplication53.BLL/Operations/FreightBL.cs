﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using WebApplication53.Core.Operations;


namespace WebApplication53.BLL.Operations
{
    public class FreightBL : IFreightBL 
    {
        private readonly FreightDbContext freightDbContext;  
        public WebApplication53.Freight AddFreights(WebApplication53.Freight freight)
        {
            freightDbContext.Freights.Add(freight);
            freightDbContext.SaveChanges();


            return freight; 
        }

        public IEnumerable GetFreights()
        {
            throw new NotImplementedException();
        }

        public bool RemoveFreights(int id)
        {
            var rmv = freightDbContext.Freights.Find(id);
            if (rmv == null)
            {
                return false;
            }

            freightDbContext.Freights.Remove(rmv);
            freightDbContext.SaveChanges();


            return true; 
        }

        public bool UpdateFreights(WebApplication53.Freight freight, int id) 
        {
            var freightToUpdate = freightDbContext.Freights.Find(id);
            if (freightToUpdate == null)
            {
                return false; 
            }


            freightToUpdate.Id = freight.Id;
            freightToUpdate.Massive = freight.Massive;
            freightToUpdate.Name = freight.Name;
            freightToUpdate.Country = freight.Country;
            freightToUpdate.City = freight.City;
            freightToUpdate.Ocean = freight.Ocean;


            freightDbContext.Freights.Update(freightToUpdate);
            freightDbContext.SaveChanges();


            return true;
        }
    }
}
