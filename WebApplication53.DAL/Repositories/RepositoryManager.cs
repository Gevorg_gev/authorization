﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WebApplication53.Core.Repositories;

namespace WebApplication53.DAL.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly FreightDbContext freightDbContext;
        public RepositoryManager(FreightDbContext freightDbContext)  
        {
            this.freightDbContext = freightDbContext;
        }

        private IFreightRepository _freights;
        public IFreightRepository Freights => _freights ?? (_freights = new FreightRepository(freightDbContext));

        private IUserRepository _users;
        public IUserRepository Users => _users ?? (_users = new UserRepository(freightDbContext));

        object IRepositoryManager.Users => throw new NotImplementedException();

        public int SaveChanges()
        {
            return freightDbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return freightDbContext.SaveChangesAsync();
        }

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(freightDbContext, isolation);
        } 
    }
}
