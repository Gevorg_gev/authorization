﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication53.Core.Entities;

namespace WebApplication53.Core.Repositories
{
    public interface IUserRepository : ISqlRepository<User> 
    {

    }
}
