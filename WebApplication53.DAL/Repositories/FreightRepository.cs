﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication53.Core.Repositories;

namespace WebApplication53.DAL.Repositories
{
    public class FreightRepository : SqlRepositoryBase<Freight> , IFreightRepository
    {
        public FreightRepository(FreightDbContext freightDbContext) : base(freightDbContext) 
        {

        }
    }
}
