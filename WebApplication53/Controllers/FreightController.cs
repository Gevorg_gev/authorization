using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication53
{
    [ApiController]
    [Route("[controller]")]
    public class FreightController : ControllerBase
    {
        private readonly FreightDbContext freightDbContext;
        public FreightController(FreightDbContext freightDbContext)
        {
            this.freightDbContext = freightDbContext;
        }

        private readonly string[] Freight_Name = new[]
        {
            "Armenian Freight" , "American Freight" , "Arabian Freight" , "Chinese Freight" 
        };

        private readonly string[] Freight_Country = new[]
        {
            "Armenia" , "USA" , "Arabia" , "China"
        };

        private readonly string[] Freight_City = new[]
        {
            "Yerevan" , "Washington" , "Al Riad" , "Beijing" 
        };

        private readonly string[] Freight_Ocean = new[] 
        {
            "Atlantic Ocean" , "Pacific Ocean" 
        };

        [HttpGet]
        public IEnumerable GetFreights()
        {
            var rng = new Random();
            return Enumerable.Range(1, 6).Select(index => new Freight
            {
                Id = rng.Next(1,6),
                Massive = rng.Next(1,2500),
                Name = Freight_Name[rng.Next(Freight_Name.Length)],
                Country = Freight_Country[rng.Next(Freight_Country.Length)],
                City = Freight_City[rng.Next(Freight_City.Length)],
                Ocean = Freight_Ocean[rng.Next(Freight_Ocean.Length)] 
            });
        }

        [HttpPost]
        public IActionResult AddFreights([FromBody] Freight freight)
        {
            freightDbContext.Freights.Add(freight);


            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveFreights([FromRoute] int id)
        {
            freightDbContext.Remove(id);


            return Ok(); 
        }

        [HttpPut("{id}")]
        public IActionResult UpdateFreights([FromBody] Freight freight, [FromRoute] int id)
        {
            freightDbContext.Update(id); 


            return Ok(); 
        }
    }
}