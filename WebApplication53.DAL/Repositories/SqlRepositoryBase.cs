﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication53.Core.Repositories;

namespace WebApplication53.DAL.Repositories
{
    public class SqlRepositoryBase<T> : ISqlRepository<T> where T : class
    {
        private readonly FreightDbContext freightDbContext;
        public SqlRepositoryBase(FreightDbContext freightDbContext)
        {
            this.freightDbContext = freightDbContext;
        }
        public T Add(T entity) 
        {
            freightDbContext.Set<T>().Add(entity);
            return entity;
        }

        public T Get()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetWhere(Func<T, bool> func)
        {
            return freightDbContext.Set<T>().Where(func);
        }

        public void Remove(T entity)
        {
            freightDbContext.Set<T>().Remove(entity);
        }

        public void Update(T entity)
        {
            freightDbContext.Set<T>().Update(entity); 
        }
    }
}
