using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication53.Core.Models;
using WebApplication53.Core.Operations;

namespace WebApplication53
{
    [ApiController]
    [Route("[controller]")] 
    public class UsersController : ControllerBase
    {
        private readonly IUserBL _userBL;

        public UsersController(IUserBL userBL)
        {
            _userBL = userBL;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] UserRegisterModel registerModel)
        {
            var user = await _userBL.RegisterAsync(registerModel, HttpContext);
            return Created("", user);
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody] UserLoginModel loginModel)
        {
            await _userBL.LoginAsync(loginModel, HttpContext);
            return Ok();
        }

        [HttpPost("logout")]
        public async Task<IActionResult> LogOutAsync() 
        {
            await _userBL.LogOutAsync(HttpContext);
            return Ok();
        } 
    }
}